(function() {

  /* global io */
  'use strict';

  angular
    .module('unzemsApp')
    .factory('socket', socket);

  /* @ngInject */
  function socket(socketFactory) {

    // socket.io now auto-configures its connection when we ommit a connection url
    var ioSocket = io(null, {
      // Send auth token on connection, you will need to DI the Auth service above
      // 'query': 'token=' + Auth.getToken()
    });

    var socket = socketFactory({
      ioSocket: ioSocket
    });

    return {

      recvMsg: recvMsg

    };

    function recvMsg(event, chats, cb) {
      cb = cb || angular.noop;
      socket.on(event + ":recv", function (item) {
        chats.push(item);
      });

    }
  }

})();
