/**
 * Created by laptop on 2015-07-30.
 */
(function () {
  angular
    .module('unzemsApp')
    .service('modal', modal);

  function modal($modal, $q) {
    this.open = open;

    function open(size, templateId, controller, params) {
      var modalInstance = $modal.open({
        templateUrl: templateId,
        controller: controller,
        size: size,
        resolve: {
          params: function(){
            return params;
          }}
      });

      var deferred = $q.defer();

      modalInstance.result.then(function(result){
        deferred.resolve(result);
      }, function(err){
        deferred.reject(err);
      });
      return deferred.promise;
    }
  }
})();
