(function() {

  'use strict';

  angular
    .module('unzemsApp')
    .service('storageService', StorageService);

  /* @ngInject */
  function StorageService() {
    this.setValue = function(key, value, options){
      angular.element.jStorage.set(key, value, options);
    }

    this.getValue = function(key){
      return angular.element.jStorage.get(key);
    }

    this.removeValue = function(key){
      angular.element.jStorage.deleteKey(key);
    }

    this.flush = function(){
      angular.element.jStorage.flush();
    }

    this.setTTL = function(key, ttl){
      angular.element.jStorage.setTTL(key, ttl);
    }
  }

})();
