/**
 * Created by laptop on 2015-08-03.
 */
(function(){
  angular
    .module('unzemsApp')
    .service('fileUploadService', fileUploadService);

  function fileUploadService($q, Upload, Auth) {
    this.uploadPhoto = uploadPhoto;

    function uploadPhoto(model, id, file, group) {
      var deferred = $q.defer();
      console.log(model);
      console.log(id);
      console.log(file);
      console.log(file.name);
      console.log(group);
      var params = {
        //url: config.api_version + '/' + model + '/' + id + '/photo',                /:id/photo
        url: '/api/v1/' + model + '/' + id + '/photo',
        method: 'PUT',
        headers: {
          'Authorization': 'Bearer ' + Auth.getToken(),
        },
        fileFormDataName: 'photo',
        file: file,
        data: group
      };

      Upload
        .upload(params)
        .progress(function (evt) {
          var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);

          console.log(progressPercentage);
          //logger.info('progress: ' + progressPercentage + '% ' + evt.config.file.name);
        }).success(function (data, status, headers, config) {
          //logger.info('file ' + config.file.name + 'uploaded. success: ' + JSON.stringify(data));
          deferred.resolve(data);
        }).error(function (data, status, headers, config) {
          //logger.info('file ' + config.file.name + 'uploaded. error: ' + JSON.stringify(data));
          deferred.reject(data);
        });

      return deferred.promise;
    }
  }
})();
