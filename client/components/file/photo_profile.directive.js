/**
 * Created by laptop on 2015-08-03.
 */
(function () {
  angular
    .module('unzemsApp')
    .directive('photoProfile', photoProfile);

  function photoProfile($rootScope, fileUploadService) {
    return {
      restrict: 'EA',
      scope: {
        profile: '=info'
      },
      template: '<div>' +
      '<img class="group_img" id="_photo" width="250px" height="150px" ng-src="{{profile.modelPhoto}}">' +
      '<div ng-if="profile.isAdmin" ngf-select ng-model="file" class="btn btn-default btn-sm" style="margin-top: 10px">Select File</div>' +
      '</div>'+
      '{{file}}',
      link: link
    };

    function link(scope, element, attrs) {
      scope.$watch('file', function () {
        if (!scope.file) {
          return;
        }
        _upload();
      });

      function _upload() {
        fileUploadService
          .uploadPhoto(scope.profile.modelName, scope.profile.modelId, scope.file)
          .then(function (response) {
            // change image
            var media = document.getElementById('_photo');
            var photo_url = config.api_version + '/' + scope.profile.modelName + '/' + scope.profile.modelId + '/photo';
            media.src = photo_url;
            $rootScope.$broadcast('profile:image:change', {id: scope.profile.modelId, photo: photo_url});
          });
      }
    }
  }
})();
