'use strict';

describe('Service: unzAlert', function () {

  // load the service's module
  beforeEach(module('unzemsApp'));

  // instantiate service
  var unzAlert;
  beforeEach(inject(function (_unzAlert_) {
    unzAlert = _unzAlert_;
  }));

  it('should do something', function () {
    expect(!!unzAlert).toBe(true);
  });

});
