(function () {

  'use strict';

  angular
    .module('unzemsApp')
    .controller('GroupListCtrl', GroupListCtrl)
    .controller('GroupViewCtrl', GroupViewCtrl)
    .controller('GroupCreateCtrl', GroupCreateCtrl)
    .controller('GroupEditCtrl', GroupEditCtrl);

  /* @ngInject */

  function GroupListCtrl($scope, $window, groupService ){
    $scope.groups = groupService.getGroups();

    $scope.deleteGroup = function(group){
      groupService.deleteGroup(group).then(function(){
        $window.location.href = '';
      }, function(error){});
    }
  }


  function GroupViewCtrl($scope, $stateParams, groupService){
    $scope.group = groupService.getGroup($stateParams.id);
  };


  function GroupCreateCtrl($scope, $state, $stateParams, groupService){
    $scope.group = groupService.newGroup();

    $scope.addGroup = function(){
      groupService.addGroup($scope.group).then(function(){
        $state.go('groups');
      }, function (error){});
    }
  }


  function GroupEditCtrl($scope, $state, $stateParams, groupService) {
    $scope.updateGroup = function(){
      groupService.updateGroup($scope.group).then(function(){
        $state.go('groups');
      });
    }

    $scope.group = groupService.getGroup($stateParams.id);
  }
})();
