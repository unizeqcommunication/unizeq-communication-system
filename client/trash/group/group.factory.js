(function () {

  //setData(groupData) : null 생성자에 데이터 세팅..
  //load(id)  : promise 서버와 데이터 동기적으로 가져옴
  'use strict';

  angular
    .module('unzemsApp')
    .factory('Group', Group);

  /* @ngInject */
  function Group() {
    return $resource('/api/v1/groups/:id', {id: '@_id'}, {
      update: {method: 'PUT'}
    });
  }
})();

