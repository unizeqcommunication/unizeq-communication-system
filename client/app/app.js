(function() {

  'use strict';

  angular
    .module('unzemsApp', [
		  'ngCookies',
		  'ngResource',
		  'ngSanitize',
		  'btford.socket-io',
		  'ui.router',
		  'ui.bootstrap',
      'ngFileUpload'])
    .config(config)
    .factory('authInterceptor', authInterceptor)
    .run(run);

  /* @ngInject */
  function config($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider) {
    $urlRouterProvider
      .otherwise('/');

    $httpProvider.interceptors.push('authInterceptor');
   // $httpProvider.interceptors.push('sgHttpInterceptor');
  }

  /* @ngInject */
  function run($rootScope, $location, Auth) {
    // Redirect to login if route requires auth and you're not logged in
    $rootScope.$on('$stateChangeStart', function (event, next) {
      Auth.isLoggedInAsync(function(loggedIn) {
        if (next.authenticate && !loggedIn) {
          $location.path('/login');
        }
      });
    });
  }
  //
    /* @ngInject */
  function authInterceptor($rootScope, $q, $cookieStore, $location, unzAlert, storageService) {
    return {
      // Add authorization token to headers
      request: function (config) {
        config.headers = config.headers || {};
        if (storageService.getValue('token')) {
          config.headers.Authorization = 'Bearer ' + storageService.getValue('token');
        }else{
         // $location.path('/login');
        }
        return config;
      },

      // Intercept 401s and redirect you to login
      responseError: function(response) {
        if(response.status === 401) {
          $location.path('/login');
          // remove any stale tokens
          storageService.removeValue('token');
          callAlert(response.data.message, response.status);
          return $q.reject(response);
        }
        else {
          callAlert(response.data.message, response.status);
          return $q.reject(response);
        }
      }
    };
    function callAlert(msg, status){
      unzAlert.error(msg, status);
    };
  }
})();
