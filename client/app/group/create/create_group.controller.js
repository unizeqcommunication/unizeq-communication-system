/**
 * Created by laptop on 2015-07-30.
 */

(function(){
  angular
    .module('unzemsApp')
    .controller('CreateGroupCtrl', CreateGroupCtrl);

  function CreateGroupCtrl($scope, $modalInstance, groupService, unzAlert){
    $scope.create = create;
    $scope.cancle = cancle;

    init();

    function init(){
      $scope.group = groupService.create();
    }

    function create(){
      groupService.addGroup($scope.group)
        .then(function(response){
          console.log(response);
          $modalInstance.close(response);
        }, function(err){
          unzAlert.error('create group error', err);
        });
    }

    function cancle(){
      $modalInstance.dismiss('cancel');
    }
  }
})();
