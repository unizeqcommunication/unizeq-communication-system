/**
 * Created by laptop on 2015-07-29.
 */
(function () {
  'use strict'

  angular
    .module('unzemsApp')
    .service('groupService', groupService);


  function groupService(Group, Auth, $q, storageService) {

    this.create = create;
    this.addGroup = addGroup;
    this.update = update;
    this.remove = remove;
    this.getGroup = getGroup;
    this.getGroups = getGroups;
    this.isGroupOwner = isGroupOwner;
    this.memberEnroll = memberEnroll;
    this.memberLeave = memberLeave;

    function create() {
      return new Group();
    }

    function addGroup(group) {
      return group.$save();
    }

    function getGroups(isMyGroup, params) {
      var deferred = $q.defer();
      var user = storageService.getValue('user')._id;
      var group;
      if (isMyGroup) {
        group = {type: 'RELATED', user: user};
      } else {
        group = {type: 'UNRELATED', user: user};
      }

      if (params) {
        params = angular.extend(group, params);
      } else {
        params = group;
      }
      Group.query(params, function (response) {
        deferred.resolve(response);
      }, function (err) {
        deferred.reject(err);
      });
      return deferred.promise;
    }

    function getGroup(_id) {
      var deferred = $q.defer();
      Group.get({id: _id}, function(response){
        deferred.resolve(response);
      }, function(err){
        deferred.reject(err);
      });
      return deferred.promise;
    }


    function isGroupOwner(group) {
      if (group && group.owner && Auth.isLoggedIn()) {


        return group.owner._id === Auth.getCurrentUser()._id;
      } else {
        return false;
      }
    }

    function memberEnroll(groupId) {
      var deferred = $q.defer();

      var userId = storageService.getValue('user')._id;
      Group.enroll({id:groupId, userId: userId}, function(response){
        deferred.resolve(response);
      }, function(err){
        deferred.reject(err);
      });

      return deferred.promise;
    }

    function memberLeave(groupId) {
      var deferred = $q.defer();
      var userId = storageService.getValue('user')._id;

      Group.withdraw({id:groupId, userId: userId}, function(response){
        deferred.resolve(response);
      },function (err){
        deferred.reject(err);
      });
      return deferred.promise;
    }

    function update(groupId, group){
      var deferred = $q.defer();
      console.log(group);
      Group.update({id:groupId, group:group}, function(response){
        deferred.resolve(response);
     }, function(err){
       deferred.reject(err);
     });

      return deferred.promise;
    }

    function remove(groupId){
      var deferred = $q.defer();

      Group.delete({id:groupId}, function(response){
        deferred.resolve(response);
      }, function(err){
        deferred.reject(err);
      });
      return deferred.promise;
    }
  }
})();
