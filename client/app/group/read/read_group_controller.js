/**
 * Created by laptop on 2015-08-03.
 */
(function () {
  'use sctrict';

  angular
    .module('unzemsApp')
    .controller('ReadGroupCtrl', ReadGroupCtrl);

  /* @ngInject */
  function ReadGroupCtrl($scope, $modalInstance, params, groupService, unzAlert, fileUploadService) {
    $scope.memberJoin = memberJoin;
    $scope.memberLeave = memberLeave;
    $scope.update = update;
    $scope.remove = remove;
    $scope.cancel = cancel;

    $scope.save = save;
    _init();

    function _init() {
      $scope.group = params.group;
      // photo-profile directive 에서 사용함
      $scope.profile = {
        modelName: 'groups',
        modelId: params.group.id,
        modelPhoto: params.group.photo,
        isAdmin: groupService.isGroupOwner(params.group),
        isMember: params.type === 'myGroup'
      }
      $scope.$watch('file', function(){
        if(!$scope.file) return;
        _upload();
      })
    };

    function memberJoin() {
      groupService
        .memberEnroll($scope.group.id)
        .then(function (response) {
          $scope.group.memberEnroll = true;
          $modalInstance.close($scope.group);
          unzAlert.success($scope.group.name + '그룹에 가입하였습니다.');
        }, function (error) {

        });
    };

    function memberLeave() {
      groupService
        .memberLeave($scope.group.id)
        .then(function (response) {
          $scope.group.memberLeave = true;
          $modalInstance.close($scope.group);
          unzAlert.success($scope.group.name + '그룹에서 탈퇴하였습니다.');
        }, function (error) {

        });
    };

    function update() {
      groupService
        .update($scope.group.id, $scope.group)
        .then(function (response) {
          $scope.group.update = true;
          $modalInstance.close(response.data);
          unzAlert.success('그룹정보를 변경하였습니다.');
        }, function (error) {
        });
    };

    function remove() {
      groupService
        .remove($scope.group.id)
        .then(function (response) {
          $scope.group.remove = true;
          $modalInstance.close($scope.group);
          unzAlert.success('그룹을 삭제하였습니다.');
        }, function (error) {
        });
    };

    function save() {

    };

    function cancel() {
      $modalInstance.dismiss('cancel');
    };

    function _upload(){
      fileUploadService.uploadPhoto('groups', $scope.group.id, $scope.file, $scope.group);
    }
  }
})();
