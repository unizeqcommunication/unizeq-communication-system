/**
 * Created by laptop on 2015-07-30.
 */

(function(){
  angular
    .module('unzemsApp')
    .factory('Group', Group);


  function Group($resource){
    return $resource('/api/v1/groups/:id', {id: '@id'},{
      'enroll' : {
        method:'PUT',
        params:{id: '@id'},
        url:'/api/v1/groups/:id/members/enroll'
      },
      'withdraw':{
        method:'PUT',
        params:{id: '@id'},
        url:'/api/v1/groups/:id/members/withdraw'
      },
      'update':{
        method:'PUT',
        params:{id: '@id'}
      }
    });
  }
})();
