(function () {

  'use strict';

  angular
    .module('unzemsApp')
    .controller('MainCtrl', MainCtrl);

  function MainCtrl($scope, $log, $state, groupService, unzAlert, modal, storageService) {

    init();
    function init() {
      _myGroups(true);
      _otherGroups(false);
    }

    $scope.openCreateModal = openCreateModal;
    $scope._myGroups = _myGroups;
    $scope._otherGroups = _otherGroups;
    $scope.showGroupDetail = showGroupDetail;

    function openCreateModal() {
      modal.open('sm', 'create_group.html', 'CreateGroupCtrl')
        .then(function (result) {
          unzAlert.success(result.name + ' 그룹이 성공적으로 생성되었습니다.', 200);


          $scope.myGroups.unshift(result);
        }, function (err) {
          unzAlert.error('그룹 생성을 취소했습니다.', 500);
        });
    }

    function _myGroups(isMyGroup, myGroupName) {
      var params = {name: myGroupName};
      groupService
        .getGroups(isMyGroup, params).then(function (data) {

          if (isMyGroup) {
            $scope.myGroups = data;
          } else {
            $scope.otherGroups = data;
          }
        });
    }

    function _otherGroups(isMyGroup, otherGroupName) {
      var params = {name: otherGroupName};
      groupService
        .getGroups(isMyGroup, params).then(function (data) {
          if (isMyGroup) {
            $scope.myGroups = data;
          } else {
            $scope.otherGroups = data;
          }
        });
    }
    function showGroupDetail(group, type) {
      modal
        .open('lm', 'read_group.html', 'ReadGroupCtrl', {group: group, type: type})
        .then(function(result){
          if(result && result.memberEnroll) {
            delete result.memberEnroll;
            $scope.otherGroups = _.without($scope.otherGroups, result);
            $scope.myGroups.unshift(result);
          }

          if(result && result.memberLeave) {
            delete result.memberLeave;
            $scope.myGroups = _.without($scope.myGroups, result);
            $scope.otherGroups.unshift(result);
          }

          if(result && result.update){
            delete result.update;
            $scope.myGroups = _.find($scope.myGroups, function(group){
              if(group.id === result.id){
                return group = result;
              }else{
                return group;
              }
            });
          }

          if(result && result.remove){
            delete result.remove;
            $scope.myGroups = _.without($scope.myGroups, result);
          }
        }, function(error) {});
    }
  }
})
();
