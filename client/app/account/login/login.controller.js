(function () {

  'use strict';

  angular
    .module('unzemsApp')
    .controller('LoginCtrl', LoginCtrl);

  /* @ngInject */
  function LoginCtrl($scope, Auth, $location, unzAlert) {

    $scope.login = login;
    $scope.showSignUp = showSignUp;

    function login(form) {

      $scope.submitted = true;

      if (form.$valid) {
        Auth.login({
          email: $scope.user.email,
          password: $scope.user.password
        }).then(function () {
          $location.path('/');
        }).catch(function (err) {
        });
      }
    }

    function showSignUp() {
      $scope.signUpShowed = !$scope.signUpShowed;
    }
  }

})();
