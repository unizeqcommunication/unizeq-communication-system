/**
 * Created by laptop on 2015-08-04.
 */


(function(){
  'use strict'

  angular
    .module('unzemsApp')
    .controller('ChatRoomCtrl', ChatRoomCtrl);

  function ChatRoomCtrl($scope, $stateParams, Auth, groupService, socket, chatRoomService){
    init();

    $scope.sendMsg = sendMsg;
    $scope.isMychat = isMychat;
    $scope.callMyPhoto = callMyPhoto;

    function init(){
      $scope.chats = [];

      $scope.$watch('chats', function(){
        if(!$scope.chats) return;
        _goToBottom();
      });

      _groupInfo();
      _getMessageList();
    }

    function sendMsg(msg) {
      var message = {
        contents : msg,
        provider : $scope.me._id,
        date_time : new Date(),
        group: $scope.group.id,
        name : $scope.me.name,
        notice : $scope.notice
      }

      chatRoomService.sendMsg(message).then(function(data){
        $scope.chats.push(message);
        $scope.chatText = "";
        _goToBottom();
      }, function(err){
        //....
      });
    }

    function _groupInfo(){

      groupService.getGroup($stateParams.id)
        .then(function (response){
          $scope.group = response;
          $scope.me = Auth.getCurrentUser();
          $scope.backgroundImage = {
            'background-image': 'url('+ $scope.group.photo+')',
            'background-repeat': 'no-repeat',
            'background-size': 'cover'
          };

          $scope.titleFont = {
            'color' : 'yellow',
            'text-shadow' : '0 0 8px #000',
            '-moz-text-shadow' : '0 0 8px #000',
            '-webkit-text-shadow' : '0 0 8px #000'
          }
        });

      $scope.isAdmin = groupService.isGroupOwner($scope.group);
    }

   function callMyPhoto(chat){
      var isMy = $scope.isMychat(chat);
     return isMy?'FF5E00':'55C1E7';
    }

    function _getMessageList(){
      chatRoomService
        .getGroupMessageList($stateParams.id)
        .then(function(data){
          $scope.chats = data;
          _goToBottom();
          socket.recvMsg($stateParams.id, $scope.chats, _goToBottom);
        });
    }

    function isMychat(chat){
      return chat.provider === $scope.me._id;
    }




    function _goToBottom(){
      $("#chatscroll").scrollTop($("#chatscroll")[0].scrollHeight);
    }
  }
})();
