(function(){
  angular
    .module('unzemsApp')
    .service('chatRoomService', chatRoomService);

  function chatRoomService(ChatRoom, $q){
    this.sendMsg = sendMsg;
    this.getGroupMessageList = getGroupMessageList;


    function sendMsg(msg){
      var deferred = $q.defer();
      ChatRoom.save(msg, function(response){
        deferred.resolve(response);
      }, function(err){
        deferred.reject(err);
      });
      return deferred.promise;
    }

    function getGroupMessageList(groupId){
      var deferred = $q.defer();
      var params = {
        id: groupId
      }
      ChatRoom.query(params, function(response){
        deferred.resolve(response);
      }, function(err){
        deferred.reject(err);
      });
      return deferred.promise;
    }
  }
})();
