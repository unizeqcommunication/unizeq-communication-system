/**
 * Created by laptop on 2015-08-06.
 */

(function(){
  angular
    .module('unzemsApp')
    .factory('ChatRoom', ChatRoom);

  function ChatRoom($resource){
    return $resource('/api/message/:id', {id:'@id'});
  }
})();
