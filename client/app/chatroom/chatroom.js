/**
 * Created by laptop on 2015-08-04.
 */
(function(){
  'use strict'
  angular
    .module('unzemsApp')
    .config(Config);
  function Config($stateProvider){
    $stateProvider
      .state('chatroom', {
        url: '/chatroom/:id',
        templateUrl:'app/chatroom/chatroom.html',
        controller: 'ChatRoomCtrl'
      });
  }
})();
