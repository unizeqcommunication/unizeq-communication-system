/**
 * Created by laptop on 2015-07-29.
 */

'use strict'

var DEFAULT_GROUP_ICON = '/images/icon_group.png';

exports.getGroupPhoto = getGroupPhoto;
exports.sortByRole = sortByRole;

function getGroupPhoto(id, has_photo) {
  return has_photo ? 'api/v1/groups/' + id + '/photo' : DEFAULT_GROUP_ICON;
}

function sortByRole(o) {
  switch (o.role) {
    case 'OWNER' :
      return 1;
    case 'MEMBER' :
      return 2;
    case 'GUEST' :
      return 3;
    default  :
      return 4;
  }
}
