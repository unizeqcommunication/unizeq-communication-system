'use strict';

var express = require('express');
var controller = require('./group.controller');
var auth = require('../../auth/auth.service');
var multiparty = require('connect-multiparty');
var multipartyMiddleware = multiparty();
var router = express.Router();

router.get('/', controller.list);
router.get('/:id', controller.read);
router.post('/', auth.isAuthenticated(), controller.create);
router.put('/:id', controller.update);
router.patch('/:id', controller.update);
router.delete('/:id', controller.destroy);

router.put('/:id/photo', multipartyMiddleware, controller.photo.upload);


// enroll
router.put('/:id/members/enroll', auth.isAuthenticated(), controller.members.enroll);

router.put('/:id/members/withdraw', auth.isAuthenticated(), controller.members.withdraw);

module.exports = router;
