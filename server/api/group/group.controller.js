'use strict';

var GroupService = require('./group.service');
var photoUtils = require('../../components/utilities/photo');


exports.list = list;
exports.read = read;
exports.create = create;
exports.update = update;
exports.destroy = destroy;

var getMember = function (type, user) {
  switch (type) {
    case 'RELATED':
      return {
        id: user,
        inverse: false,
        type: type
      };
    case 'UNRELATED':
      return {
        id: user,
        inverse: true,
        type: type
      };
    case 'ALL':
    case undefined:
      return {};
    default:
    //throw new errors.ParamInvalidError('type');
  }
};

function list(req, res, next) {
  console.log(req.query.user);
  var member = getMember(req.query.type, req.query.user); //req.login.id);

  var options = {
    name: req.query.name,
    type: member.type,
    member: {
      id: member.id,
      inverse: member.inverse
    }
  };

  GroupService
    .list(options)
    .then(function (groups) {
      res.json(200, groups);
    })
    .catch(function (err) {
      next(err);
    });
};

function read(req, res, next) {
  GroupService
    .read(req.params.id)
    .then(function (group) {
      res.json(200, group);
    })
    .catch(function (err) {
      // if(err.code === 'GROUP_NOT_FOUND') {
      //   return res.send(404);
      // }
      // res.send(500, err);
      next(err);
    });
};
function create(req, res, next) {
  GroupService
    .create(req.body, req.user)
    .then(function (group) {
      console.log(group);
      res.json(201, group);
    })
    .catch(function (err) {
      next(err);
    });
};
function update(req, res, next) {
  var group = req.body.group;

  if(group.owner) delete group.owner;
  if(group.id) delete group.id;

  GroupService
    .update(req.params.id, group)
    .then(function (group) {
      res.json(201, group);
    })
    .catch(function (err) {
      // if(err.code === 'GROUP_NOT_FOUND') {
      //   return res.send(404);
      // }
      // res.send(500, err);
      next(err);
    });
};
function destroy(req, res, next) {
  GroupService
    .destroy(req.params.id)
    .then(function (group) {
      res.json(204);
    })
    .catch(function (err) {
      // if(err.code === 'GROUP_NOT_FOUND') {
      //   return res.send(404);
      // }
      // res.send(500, err);
      next(err);
    });
};


//exports.photo = {
//  upload: function (req, res, next) {
//    var groupId = req.params.id;
//    var photo = req.files.photo;
//
//    GroupService.read(groupId).then(function(group){
//      GroupService.photo.upload(group, photo)
//        .then(function (data) {
//          res.writeHead('200', {'Content-Type': 'image/png'});
//          res.end(group.photo, 'base64');
//        })
//        .catch(function (err) {
//          next(err);
//        });
//    });
//  }
//}


exports.photo = {
  upload: function(req, res, next){
    var groupId = req.params.id;
    var photo = req.files.photo;
    GroupService.read(groupId).then(function(group){
      if(photo && photo.path){
        photoUtils.read(photo.path, 600)
          .then(function(data){
            return GroupService.photo.upload(group, data);
          })
          .then(function(data){
            res.writeHead('200', {'Content-Type': 'image/png'});
            res.end(group.photo, 'base64');
          })
          .catch(function(err){
            next(err);
          });
      } else{

      }
    });
  }
}


exports.members = {
  enroll: function (req, res, next) {
    GroupService.members
      .enroll(req.params.id, req.body.userId)
      .then(function(group) {
        res.json(201, group);
      }).catch(function(err) {
        next(err);
      });
  },
  withdraw: function(req, res, next) {
    GroupService.members
      .withdraw(req.params.id, req.body.userId)
      .then(function() {
        res.end();
      }).catch(function(err) {
        next(err);
      });
  }
};

//exports.photo = {
//  upload: function(req, res, next) {
//    var group = req.group;
//    var photo = req.files.photo;
//
//    if (photo && photo.path) {
//      // 600 파일의 width 조정
//      photoUtils.read(photo.path, 600)
//        .then(function(data) {
//          return GroupService.photo.upload(group, data);
//        })
//        .then(function(group) {
//          res.writeHead('200', { 'Content-Type': 'image/png' });
//          res.end(group.photo, 'base64');
//        })
//        .catch(function(err) {
//          next(err);
//        });
//    } else {
//      next(new errors.PhotoRequiredError());
//    }
//  },
//
//  download: function(req, res, next) {
//    GroupService
//      .photo.download(req.group)
//      .then(function(group) {
//        var photo = group.photo;
//        var hash = util.calculateHash(photo);
//
//        if (util.isNotModified(req, hash)) {
//          res.writeHead('304', {
//            'ETag': hash,
//            'Cache-Control': 'public, max-age=86400'
//          });
//          res.end();
//        } else {
//          res.writeHead('200', {
//            'Content-Type': 'image/png',
//            'ETag': hash,
//            'Cache-Control': 'public, max-age=86400'
//          });
//          res.end(photo, 'base64');
//        }
//      }).catch(function(err) {
//        next(err);
//      });
//  }
//};

//exports.members = {
//  enroll: function (req, res, next) {
//    GroupService.members
//      .enroll(req.group, req.user)
//      .then(function() {
//        res.finish();
//      }).catch(function(err) {
//        next(err);
//      });
//  },
//  leave: function(req, res, next) {
//    GroupService.members
//      .leave(req.group, req.user)
//      .then(function() {
//        res.finish();
//      }).catch(function(err) {
//        next(err);
//      });
//  }
//};

// Get list of group

// Get a single group

// Creates a new group in the DB.

// Updates an existing group in the DB.

// Deletes a group from the DB.

// member auto enroll


//'use strict';
//
//var GroupService = require('./group.service');
//
//exports.index = index;
//exports.show = show;
//exports.create = create;
//exports.update = update;
//exports.destroy = destroy;
//
//// Get list of group
//function index(req, res) {
//  GroupService
//    .index()
//
//    .then(function(groups) {
//      res.json(200, groups);
//    })
//    .catch(function(err) {
//      res.send(500, err);
//    });
//};
//
//// Get a single group
//function show(req, res) {
//  GroupService
//    .show(req.params.id)
//
//    .then(function(group) {
//      res.json(group);
//    })
//    .catch(function(err) {
//      if(err.code === 'NOT_FOUND') {
//        return res.send(404);
//      }
//      res.send(500, err);
//    });
//};
//
//// Creates a new group in the DB.
//function create(req, res) {
//  console.log(req.body);
//  GroupService
//    .create(req.body, req.user)
//
//    .then(function(group) {
//      res.json(201, group);
//    })
//    .catch(function(err) {
//      console.log(err);
//      res.send(500, err);
//    });
//};
//
//// Updates an existing group in the DB.
//function update(req, res) {
//  if(req.body._id) { delete req.body._id; }
//
//  GroupService
//    .update(req.params.id, req.body)
//
//    .then(function(group) {
//      res.json(201, group);
//    })
//    .catch(function(err) {
//      if(err.code === 'NOT_FOUND') {
//        return res.send(404);
//      }
//      res.send(500, err);
//    });
//};
//
//// Deletes a group from the DB.
//function destroy(req, res) {
//  GroupService
//    .destroy(req.params.id)
//
//    .then(function(group) {
//      res.send(204);
//    })
//    .catch(function(err) {
//      if(err.code === 'NOT_FOUND') {
//        return res.send(404);
//      }
//      res.send(500, err);
//    });
//};
//
//
