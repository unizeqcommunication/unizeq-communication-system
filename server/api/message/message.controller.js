/**
 * Created by laptop on 2015-08-07.
 */

var MessageService = require('./message.service');
var UserService = require('../user/user.service');
var _ = require('lodash');

exports.create = create;
exports.register = register;
exports.read = read;

function create(req, res, next) {
  var message = req.body;
  console.log(message.notice);
  MessageService.create(message)
    .then(function (data) {
        res.json(200, data);
        socket2.broadcast.emit(data.group + ':recv', data);
        console.log("data.group : "+data.notice);
      })
    .catch(function (err) {
      next(err);
    });
}

var socket2;
function register(socket) {
  socket2 = socket;
}


function read(req, res, next){
  var id = req.params.id;

  MessageService.read(id)
    .then(function(data){
      res.json(200, data);
    })
    .catch(function (err){
      next(err);
    });
}

