/**
 * Created by laptop on 2015-08-06.
 */
'use strict';

var express = require('express');
var controller = require('./message.controller');

var router = express.Router();

router.post('/', controller.create);
router.get('/:id', controller.read);
module.exports = router;
