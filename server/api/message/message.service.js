/**
 * Created by laptop on 2015-08-07.
 */

var Q = require('q');
var Message =  require('./message.model');

exports.create = create;
exports.read = read;
function create(msg){
  var deferred = Q.defer();
  Message.create(msg, function(err, message){
    if(err) return deferred.reject(err);
    deferred.resolve(message);
  });

  return deferred.promise;
}


function read(id){
  var deferred = Q.defer();

  var query = Message.find();

  query.where('group').equals(id);

  query.exec(function(err, messages){
    if(err) return deferred.reject(err);
    deferred.resolve(messages);
  });

  return deferred.promise;
}
