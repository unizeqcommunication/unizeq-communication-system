/**
 * Created by laptop on 2015-08-06.
 */

'use strict';

var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var MessageSchema = new Schema({
  provider : {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  name : 'String',
  group : {
    type: Schema.Types.ObjectId,
    ref : 'Group'
  },
  contents : 'String',
  date_time : Date,
  notice : {
    type: Boolean
  }
});

module.exports = mongoose.model('Message', MessageSchema);
