/**
 * Created by laptop on 2015-07-28.
 */
var passport = require('passport');
var User = require('./user.model');
var config = require('../../config/environment');
var jwt = require('jsonwebtoken');
var q = require('q');
var _ = require('lodash');


exports.index = index;
exports.show = show;
exports.update = update;
exports.create = create;
exports.destroy = destroy;
exports.changePassword = changePassword;
exports.me = me;
//전체 검색
function index() {
  var deferred = q.defer();

  User.find({}, '-salt -hashedPassword', function (err, users) {
    if (err) return deferred.reject(err);
    deferred.resolve(users);
  });

  return deferred.promise;
};




//단일 검색
function show(userId) {
  var deferred = q.defer();

  User.findById(userId, function (err, user) {
    if (err) return deferred.reject(err);
    if (!user) return deferred.reject(
      Error.new({
        code: 'NOT_FOUND',
        message: 'User : ' + userId + 'is not found.'
      })
    );

    deferred.resolve(user);
  });
  return deferred.promise;
};

function update(id, params) {
  var deferred = q.defer();

  User.findById(id, function (err, user) {
    if (err) return deferred.reject(err);
    if (!user) return deferred.reject(
      Error.new({
        code: 'NOT_FOUND',
        message: 'User : ' + id + 'is not found.'
      })
    );

    var updated = _.merge(user, params);
    updated.save(function (err) {
      if (err) {
        return deferred.reject(err);
      }
      return deferred.resolve(user);
    });
  });
  return deferred.promise;
};

function destroy(id) {
  var deferred = q.defer();

  User.findById(id, function (err, user) {
    if (err) return deferred.reject(err);
    if (!user) return deferred.reject(Error.new({
        code: 'NOT_FOUND',
        message: 'User' + id + ' is not found.'
      })
    );

    user.remove(function(err){
      if(err) return deferred.reject(err);
      return deferred.resolve(204);
    })
  });
  return deferred.promise;
};

function me(req, res, next) {
  var userId = req.user._id;
  User.findOne({
    _id: userId
  }, '-salt -hashedPassword', function (err, user) { // don't ever give out the password or salt
    if (err) return next(err);
    if (!user) return res.json(401);
    res.json(user);
  });
};


//function destroy(req, res) {
//  User.findByIdAndRemove(req.params.id, function(err, user) {
//    if(err) return res.send(500, err);
//    return res.send(204);
//  });
//};

/**
 * Change a users password
 */
function changePassword(req, res, next) {
  var userId = req.user._id;
  var oldPass = String(req.body.oldPassword);
  var newPass = String(req.body.newPassword);

  User.findById(userId, function (err, user) {
    if(user.authenticate(oldPass)) {
      user.password = newPass;
      user.save(function(err) {
        if (err) return validationError(res, err);
        res.send(200);
      });
    } else {
      res.send(403);
    }
  });
};

function create(req, res, next) {
  var newUser = new User(req.body);
  newUser.provider = 'local';
  newUser.role = 'user';
  newUser.save(function(err, user) {
    if (err) return validationError(res, err);
    var token = jwt.sign({_id: user._id }, config.secrets.session, { expiresInMinutes: 60*5 });
    res.json({ token: token });
  });
};

var validationError = function(res, err) {
  return res.json(422, err);
};


//
////유저 생성
//function create(params) {
//  var deferred = q.defer();
//
//  User.create(params, function (err, user) {
//    if (err) return deferred.reject(err);
//    deferred.resolve(user);
//  });
//  return deferred.promise;
//};
//
//
////유저 업데이트
//function update(id, params) {
//  var deferred = q.defer();
//
//  User.findById(id, function (err, user) {
//    if (err) return deferred.reject(err);
//    if (!user) return deferred.reject(
//      Error.new({
//        code: 'NOT_FOUND',
//        message: 'User : ' + id + 'is not found.'
//      })
//    );
//
//    var updated = _.merge(user, params);
//    updated.save(function (err) {
//      if (err) {
//        return deferred.reject(err);
//      }
//      return deferred.resolve(user);
//    });
//  });
//  return deferred.promise;
//};
//
//
//function destroy(id) {
//  var deferred = q.defer();
//
//  User.findById(id, function (err, user) {
//    if (err) return deferred.reject(err);
//    if (!user) return deferred.reject(Error.new({
//        code: 'NOT_FOUND',
//        message: 'User' + id + ' is not found.'
//      })
//    );
//
//    user.remove(function(err){
//      if(err) return deferred.reject(err);
//      return deferred.resolve(204);
//    })
//  });
//  return deferred.promise;
//};
